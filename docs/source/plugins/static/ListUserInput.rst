user_input
==========

Description
###########


Usage
#####

.. code-block:: bash

  asthook <app> --user_input

.. asciinema:: ListUserInput.cast
  :preload:
