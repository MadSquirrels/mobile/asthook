list_funcs
==========

Description
###########


Usage
#####

.. code-block:: bash

  asthook <app> --list_funcs <class> <func>

.. asciinema:: ListFuncs.cast
  :preload:
