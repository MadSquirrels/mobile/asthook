aws
===

Description
###########

Extract AWS credential


Usage
#####

.. code-block:: bash

  asthook <app> --aws

..
