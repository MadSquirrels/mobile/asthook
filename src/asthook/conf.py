
from pathlib import Path
import os

DIR=f"{Path.home()}/.asthook/"
PACKAGE_PATH=os.path.dirname(os.path.realpath(__file__))
VERSION="1.0.17"
