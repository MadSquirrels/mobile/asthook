Java.perform(function()
{
    var class_hook = Java.use('com.amazonaws.auth.BasicSessionCredentials');
    class_hook.$init.implementation = function (access, secret, session) {
        send(JSON.stringify({"access":access,"secret":secret,"session":session}));
        var ret = this.$init(access, secret, session);
        return ret
    };
    
    var class_hook = Java.use('com.amazonaws.auth.BasicAWSCredentials');
    class_hook.$init.implementation = function (access, secret) {
        send(JSON.stringify({"access":access,"secret":secret}));
        var ret = this.$init(access, secret);
        return ret
    };
});
